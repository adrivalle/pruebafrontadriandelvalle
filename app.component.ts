import { Component } from '@angular/core';
import axios from 'axios';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

//const axios = require('axios');

export class AppComponent {
  recetas;
  onChange($event) {
    var nombreReceta = $event.target.value;

    axios.get("http://www.recipepuppy.com/api/?i="+nombreReceta)
      .then(function (response) {
          console.log(response);
          if(response.data != null && response.data!=""){
            this.recetas = JSON.parse(response.data).results;
          }
          
        })
        .catch(function (error) {
          console.log(error);
        });
  }
}
